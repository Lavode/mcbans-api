# Copyright (C) 2012 Michael Senn "Morrolan"
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# API docs: http://help.mcbans.com/kb/plugin-information/api-documentation-contd

import urllib.parse
import urllib.request
import json
import logging
import socket
# Todo: In _do_api_request: socket.setdefaulttimeout()
# Todo: In _do_api_request: json.loads() throws an error if invalid JSON is received, might want to catch that one.

class APIManager(object):
    """A class which allows to interact with MCBans' API.

    Functions:
    get_alt_list
    add_local_player_ban
    add_global_player_ban
    add_player_ban
    add_temporary_player_ban
    remove_player_ban
    add_local_ip_ban
    add_ip_ban
    add_temporary_ip_ban
    remove_ip_ban
    get_player_info
    player_connect
    player_disconnet
    confirm_account

    """
    # Link to where the API resides, with {0} being the place for the user's API key.
    APIPOSTURL = "http://api.mcbans.com/v2/{0}"

    # List of valid bantypes.
    _ValidBanTypes =   {"local": 0,
                        "global": 1}
    _ValidTempbanMeasurements = {
                                "minute": "m",
                                "minutes": "m",
                                "hour": "h",
                                "hours": "h",
                                "day": "d",
                                "days": "d"
                                }

    def __init__(self, apikey=None, api_post_url=None):
        """Initializes the APIManager, possibly overwriting the built-in API-URL.

        Parameters:
        apikey=None -- The API key which to use. Can also be set later.
        api_post_url=NoneL -- If set to anything other than None the specified URL will be used as the location of the API.

        """
        self._APIKey = apikey
        if api_post_url is None:
            self._APIPOSTURL = APIManager.APIPOSTURL
        else:
            self._APIPOSTURL = api_post_url

        # Setting the timeout to something reasonable.
        socket.setdefaulttimeout(10)

    def _get_APIKey(self):
        return self._APIKey

    def _set_APIKey(self, value):
        if type(value) != str:
            raise TypeError("APIKey must be a string.")
        self._APIKey = value
        self._APIPOSTURL = APIManager.APIPOSTURL.format(self.APIKey)

    APIKey = property(_get_APIKey, _set_APIKey)


    def get_alt_list(self, player):
        """Returns the alternative accounts of a player. Premium-only function. UNTESTED.

        Parameters
        player -- The player whose alternative accounts to look up.

        """
        # Todo: Can't be found in the API docs anymore, might be a thing of the past now.
        args =  {
            "exec": "altList",
            "player": str(player)
                }

        raise NotImplementedError("This functionality has yet to be added.")


    def add_local_player_ban(self, issuer, player, reason):
        """Locally bans the specified player.

        Parameters:
        issuer -- The admin who issued the ban.
        player -- The player whom to ban.
        reason -- The ban reason.

        Returns:
        bool -- True if it was successful, False if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        return self.add_player_ban(issuer, player, reason, type=0)

    def add_global_player_ban(self, issuer, player, reason):
        """Globally bans the specified user.

        Parameters:
        issuer -- The admin who issued the ban.
        player -- The player whom to ban.
        reason -- The ban reason.

        Returns:
        bool -- True if it was successful, False if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """
        return self.add_player_ban(issuer, player, reason, type=1)

    def add_player_ban(self, issuer, player, reason, type=0):
        """Bans the specified user.

        Parameters:
        issuer -- The admin who issued the ban.
        player -- The player whom to ban.
        reason -- The ban reason.

        type -- 0 or 'local' for a local ban, 1 or 'global' for a global ban

        Returns:
        bool -- True if it was successful, False if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        ValueError -- Will be raised if you supplied an invalid ban type.

        """

        if type not in APIManager._ValidBanTypes.values():
            try:
                type = APIManager._ValidBanTypes[type]
            except KeyError:
                raise ValueError("Invalid ban type supplied.")

        if type == 0:
            command = "localBan"
        elif type == 1:
            command = "globalBan"

        args = {
            "exec": command,
            "admin": issuer,
            "reason": reason,
            "player": player
        }

        result = self._do_api_request(self._APIPOSTURL, args)

        #if "error" in result:
        #    raise APIError(args, result)

        if result["result"] == "y":
            return True
        elif result["result"] == "n":
            raise APIError(args, result, "Player was already banned!")
        elif result["result"] == "e":
            raise APIError(args, result)

        return False

    def add_temporary_player_ban(self, issuer, player, reason, duration, measure):
        """Temporary bans the specified player.

        Parameters:
        issuer -- The admin who issued the ban.
        player -- The player whom to ban.
        reason -- The ban reason.
        duration -- The duration the ban should last.
        measure -- The measure of the duration. Can be m(inutes), h(ours) or d(ays).

        Returns:
        bool -- True if it was successful, False if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        ValueError -- Will be raised if you supplied an invalid ban type.

        """
        if measure not in APIManager._ValidTempbanMeasurements.values():
            try:
                measure = APIManager._ValidTempbanMeasurements[measure]
            except KeyError:
                raise ValueError("You supplied an invalid measure!")

        args =  {
            "exec": "localBan",
            "admin": issuer,
            "player": player,
            "reason": reason,
            "duration": duration,
            "measure": measure
                }

        result = self._do_api_request(self._APIPOSTURL, args)

        #if "error" in result:
        #    raise APIError(args, result)

        if result["result"] == "y":
            return True
        elif result["result"] == "n":
            raise APIError(args, result, "Player was already banned!")
        elif result["result"] == "e":
            raise APIError(args, result)

        return False

    def remove_player_ban(self, issuer, player):
        """Unbans the specified user.

        Parameters:
        issuer -- The admin who removed the ban.
        player -- The player whose ban to remove.

        Returns:
        bool -- True if it was successful, false if it failed.

        Exceptions:
        APIError -- Will be raised, if the API call failed.

        """
        args = {
            "exec": "unBan",
            "admin": issuer,
            "player": player
        }

        result = self._do_api_request(self._APIPOSTURL, args)

        #if "error" in result:
        #    raise APIError(args, result)

        if result["result"] == "y":
            return True
        elif result["result"] == "n":
            raise APIError(args, result, "Player was already banned!")
        elif result["result"] == "e":
            raise APIError(args, result)

        return False


    def add_local_ip_ban(self, issuer, ip, reason):
        """Locally bans the specified IP. IS CURRENTLY NOT WORKING!

        Parameters:
        issuer -- The admin who issued the ban.
        reason -- The ban reason.
        ip -- The IP which to ban.

        Returns:
        Boolean -- True if it was successful, false if it failed.

        Exceptions:
        APIError -- Will be raised, if the API call failed.

        """
        # Not that there are any other ways you can ban an IP, but it's here for structure's sake.
        # Todo: Returns result: e at the moment. Used to work though.
        return self.add_ip_ban(issuer, reason, ip)

    def add_ip_ban(self, issuer, ip, reason):
        """Bans the specified IP. IS CURRENTLY NOT WORKING!

        Parameters:
        issuer -- The admin who issued the ban.
        reason -- The ban reason.
        ip -- The IP which to ban.

        Returns:
        Boolean -- True if it was successful, false if it failed.

        Exceptions:
        APIError -- Will be raised, if the API call failed.

        """
        # Todo: Returns result: e at the moment. Used to work though.
        command = "localBan"

        args = {
            "exec": command,
            "admin": issuer,
            "reason": reason,
            "playerip": ip
        }

        result = self._do_api_request(self._APIPOSTURL, args)

        #if "error" in result:
        #    raise APIError(args, result)

        if result["result"] == "y":
            return True
        elif result["result"] == "n":
            raise APIError(args, result, "Player was already banned!")
        elif result["result"] == "e":
            raise APIError(args, result)

        return False

    def add_temporary_ip_ban(self, issuer, ip, reason, duration, measure):
        """Temporary bans the specified player.

        Parameters:
        issuer -- The admin who issued the ban.
        ip -- The IP which to ban.
        reason -- The ban reason.
        duration -- The duration the ban should last.
        measure -- The measure of the duration. Can be m(inutes), h(ours) or d(ays).

        Returns:
        bool -- True if it was successful, False if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        ValueError -- Will be raised if you supplied an invalid ban type.

        """
        if measure not in APIManager._ValidTempbanMeasurements.values():
            try:
                measure = APIManager._ValidTempbanMeasurements[measure]
            except KeyError:
                raise ValueError("You supplied an invalid measure!")

        args =  {
            "exec": "localBan",
            "admin": issuer,
            "playerip": ip,
            "reason": reason,
            "duration": duration,
            "measure": measure
                }

        result = self._do_api_request(self._APIPOSTURL, args)

        #if "error" in result:
        #    raise APIError(args, result)

        if result["result"] == "y":
            return True
        elif result["result"] == "n":
            raise APIError(args, result, "Player was already banned!")
        elif result["result"] == "e":
            raise APIError(args, result)

        return False

    def remove_ip_ban(self, issuer, ip):
        """Removes a ban.

        Parameters:
        issuer -- The admin who removed the ban.
        ip -- The ip whose ban to remove.

        Returns:
        Boolean -- True if it was successful, false if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        args = {
            "exec": "unBan",
            "admin": issuer,
            "playerip": ip
        }

        result = self._do_api_request(self._APIPOSTURL, args)

        #if "error" in result:
        #    raise APIError(args, result)

        if result["result"] == "y":
            return True
        elif result["result"] == "n":
            raise APIError(args, result, "Player was already banned!")
        elif result["result"] == "e":
            raise APIError(args, result)
        return False


    def get_player_info(self, issuer, player):
            """Fetches a player's bans and reputation.

            Parameters:
            issuer -- The admin who looked up the player.
            player -- The player whose information to fetch.

            Returns:
            dict -- {"Bancount": int, "GlobalBanlist": [{"Reason": str, "Server": str}], "Reputation": float, "LocalBanlist": [{"Reason": str, "Server": str}], "Player": str}
            Exceptions:
            APIError -- Will be raised if the API call failed.

            """

            args =  {
                    "exec": "playerLookup",
                    "player": str(player),
                    "admin": str(issuer)
                    }

            input = self._do_api_request(self._APIPOSTURL, args)

            if "error" in input:
                raise APIError(args, input)

            # Parse the dictionary we got and create one which is easier to read.
            result = {"Bancount": None, "GlobalBanlist": [], "Reputation": None, "LocalBanlist": [], "Player": None}

            for node in input:
                if node == "global":
                    banlist = []
                    # The list containing the bans looks something like ['ultimateminecraft.net .:. griefing, lb proof', 'minecraft.lethal-zone.eu .:. griefing', 'mine.catserve.net .:. griefing lb proof']
                    for ban in input["global"]:
                        x = {}
                        paramlist = ban.split(".:.")
                        x["Server"] = paramlist[0].strip()
                        x["Reason"] = paramlist[1].strip()
                        # This is where the individual ban entry has been parsed.
                        banlist.append(x)
                        # This is where all the global bans have been parsed
                    result["GlobalBanlist"].extend(banlist)

                elif node == "total":
                    result["Bancount"] = input["total"]

                elif node == "local":
                    banlist = []
                    for ban in input["local"]:
                        x = {}
                        paramlist = ban.split(".:.")
                        x["Server"] = paramlist[0].strip()
                        x["Reason"] = paramlist[1].strip()
                        # This is where the individual ban entry has been parsed.
                        banlist.append(x)
                        # This is where all the local bans have been parsed.
                    result["LocalBanlist"].extend(banlist)

                elif node == "other":
                    # As of today, 22.04.2012, this entry is simply an empty list.
                    pass

                elif node == "reputation":
                    result["Reputation"] = input["reputation"]

            # Here the whole parsing of the API query is done. Now the player's name gets added to the result and the dictionary gets returned.
            result["Player"] = args["player"]

            return result


    def player_connect(self, player, ip):
        """Should be sent when a player connects.

        Parameters:
        player -- The connecting player.
        ip -- The IP of the connecting player.

        Returns:

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        args = {"exec": "playerConnect",
                "player": player,
                "playerip": ip}

        input = self._do_api_request(self._APIPOSTURL, args)

        result = {"DisputeCount": None,
                  "AltCount": None,
                  "AltList": [],
                  "BanStatus": None,
                  "ConnectMessage": None,
                  "GlobalBanlist": [],
                  "LocalBanlist": [],
                  "Reputation": None,
                  "isMCBansMod": None}

        for node in input:
            if node == "disputeCount":
                result["DisputeCount"] = input[node]
            elif node == "altCount":
                result["AltCount"] = input[node]
            elif node == "altList":
                result["AltList"] = input[node]
            elif node == "banStatus":
                result["BanStatus"] = input[node]
            elif node == "connectMessage":
                result["ConnectMessage"] = input[node]
            elif node == "globalBans":
                result["GlobalBanlist"] = input[node]
            elif node == "localBans":
                result["LocalBanlist"] = input[node]
            elif node == "playerRep":
                result["Reputation"] = input[node]
            elif node == "is_mcbans_mod":
                if input[node] == "y":
                    result["isMCBansMod"] = True
                else:
                    result["isMCBansMod"] = False


    def player_disconnect(self, player):
        """Should be sent when a player disconnects.

        Parameters:
        player -- The player who disconnected.

        Returns:
        Boolean -- True if it was successful, false if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        args = {"exec": "playerDisconnect",
                "player": player}

        result = self._do_api_request(self._APIPOSTURL, args)

        if result["result"] == "y":
            return True
        return False


    def confirm_account(self, player, confirmation_string):
        """Allows a new user to confirm their account. UNTESTED!

        Parameters:
        player -- The username of the account.
        confirmation_string -- The confirmation string.

        Returns:
        Boolean -- True if it was successful, false if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        args = {"exec": "playerSet",
                "player": player,
                "string": confirmation_string}

        result = self._do_api_request(self._APIPOSTURL, args)

        if result["result"] == "n":
            raise APIError(args, result, "Invalid confirmation string.")
        return True


    def _do_api_request(self, url, args):
        apiurl = str(url)

        data = urllib.parse.urlencode(args)
        # Encoding with UTF8, while it's not specified what encoding they want it worked so far. Chances are it's just ASCII.
        data = data.encode("utf8")

        request = urllib.request.Request(url, data)
        response = urllib.request.urlopen(request)
        result = response.read()

        # Decoding the result again.
        result = result.decode("utf8")

        # The result is still JSON, decoding that one so we get proper dictionaries / lists to work with.
        result = json.loads(result)

        return result



class PlayerInfo(object):
    def __init__(self, player=None, ip=None):
        """Initializes the PlayerInfo object, possibly using the supplied username/IP.

        Parameters
        player=None -- The player whose information this object will contain.
        ip=None -- The IP whose information this object will contain.

        """
        self.Player = player
        self.IP = ip
        self.Bancount = None
        self.Reputation = None
        self.LocalBanlist = []
        self.GlobalBanlist = []

    def _set_Banlist(self, value):
        pass

    def _get_Banlist(self):
        result = []
        result.extend(self.LocalBanlist)
        result.extend(self.GlobalBanlist)
        return result

    Banlist = property(_get_Banlist, _set_Banlist)

    def __repr__(self):
        return self.pretty()

    def pretty(self, separator="------"):
        """Returns a print-ready representation of the player's information.

        Parameters:
        separator(="------") -- The separator which is used to separate individual ban entries.

        Returns:
        str -- Print-ready representation of the information regarding the player.
        Exceptions:

        """

        ban_string_list = []
        note_string_list = []

        info_list = [
            "Player: " + str(self.Player),
            "Bancount: " + str(self.Bancount),
            "Reputation: " + str(self.Reputation),
            ]
        info_string = "\n".join(info_list)

        for ban in self.LocalBanlist:
            ban_string_list.append(ban.pretty())
        for ban in self.GlobalBanlist:
            ban_string_list.append(ban.pretty())
        ban_string = ("\n" + separator + "\n").join(ban_string_list)

        return ("\n" + separator*3 + "\n").join([info_string, ban_string])

    def import_bans_from_dictionary(self, d, override=False):
        """Imports ban details from a dictionary.

        Parameters:
        d -- The dictionary containing the ban details.
        override -- Whether to override the current ban list or append to it.

        Returns:

        Exceptions:

        """
        if override:
            self.LocalBanlist = []
            self.GlobalBanlist = []

        try:
            self.Bancount = d["Bancount"]
            self.Reputation = d["Reputation"]
            self.Player = d["Player"]

            for banentry in d["LocalBanlist"]:
                ban = Ban()
                ban.import_from_dictionary(banentry)
                ban.Player = self.Player
                ban.IP = self.IP
                ban.BanType = "local"
                self.LocalBanlist.append(ban)

            for banentry in d["GlobalBanlist"]:
                ban = Ban()
                ban.import_from_dictionary(banentry)
                ban.Player = self.Player
                ban.IP = self.IP
                ban.BanType = "global"
                self.GlobalBanlist.append(ban)

        except KeyError:
            logging.warning("\n".join(["PlayerInfo/import_bans_from_dictionary", "A dictionary key was missing, the result will be incomplete!", str(d)]))



class Ban(object):
    ValidBanTypes = ["local","global","temporary"]

    def __init__(self):
        self.Player = None
        self.IP = None
        self.Server = None
        self.Reason = None
        self._BanType = None

    def __repr__(self):
        return self.pretty()

    def pretty(self):
        l =  [
            "Reason: " + str(self.Reason),
            "Server: " + str(self.Server),
            "Ban type: " + str(self.BanType)
        ]

        return "\n".join(l)

    def import_from_dictionary(self, d):
        """Imports ban details from a dictionary.

        Parameters:
        d -- The dictionary containing the ban details.

        Returns:

        Exceptions:

        """
        try:
            self.Reason = d["Reason"]
            self.Server = d["Server"]
        except KeyError:
            logging.warning("\n".join(["Ban/import_from_dictionary", "A dictionary key was missing, the result will be incomplete!", str(d)]))

    def _set_BanType(self, Value):
        if Value not in Ban.ValidBanTypes:
            raise ValueError("BanType had an invalid value: " + Value)
        self._BanType = Value

    def _get_BanType(self):
        return self._BanType

    BanType = property(_get_BanType, _set_BanType)

class APIError(Exception):
    def __init__(self, query, result, message=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.Query = query
        self.Result = result
        self.Message = message
