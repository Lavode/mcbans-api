# Copyright (C) 2012 Michael Senn "Morrolan"
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


# The following code excerpts should help you to get an idea on how to use this library. There are many more functions
# than are shown here, so make sure to chek the documentation as well.


import mcbansapi
import logging

# Setting up the logger, which is where the library will send its output to.
logging.basicConfig(filename="mcbansapi.log",level=logging.WARNING)
# APIManager is the one class which you will use to interfere with the API.
# Do yourself a favour and set the APIKey at the very beginning, so you won't run into issues later on.
manager = mcbansapi.APIManager()
manager.APIKey = "YourAPIKeyGoesHere"

def import_bans_from_file(filepath, issuer, reason="Imported"):
    # Takes a file with one username per line and locally bans everyone in there, can be used to, for example, import bans from the bukkit-internal one.
    ban_list = []

    ban_file = open(filepath)
    for line in ban_file:
        ban_list.append(line)
    ban_file.close()

    for player in ban_list:
        try:
            manager.add_local_player_ban(issuer, player, reason)
        except mcbansapi.APIError as e:
            print("Whoops, an error occured while importing bans!")
            print("Query: ", e.Query)
            print("Result: ", e.Result)

def query_player_bans(player):
    try:
        result_dict = manager.get_player_info(player)
    except mcbansapi.APIError as e:
        print("An error occurred while querying for bans.")
        print("Query: ", e.Query)
        print("Result: ", e.Result)
        pass

    # Now you can either work with that dictionary directly.
    print("Player has ", result_dict["Bancount"], " bans, on the following servers:")
    for  ban in result_dict["GlobalBanlist"]:
        print(ban["Server"])
    for ban in result_dict["LocalBanlist"]:
        print(ban["Server"])

    # Or you could use the PlayerInfo class, which is more comfortable, but needlessly complicated if you just want to check something basic.
    pinfo = mcbansapi.PlayerInfo()
    pinfo.import_bans_from_dictionary(result_dict)

    print("Player has ", pinfo.Bancount, " bans, on the following servers:")
    for  ban in pinfo.Banlist:
        print(ban.Server)
